@extends('layouts.admin')
@section('title', 'Edit Order')
@section('card_name', 'Edit Order')
@section('breadcrumb')
@endsection
@section('action')
    <a href="{{route('orders.index')}}" class="btn btn-warning round btn-glow px-2"><i
            class="la la-back"></i>
        Back
    </a>
@endsection
@section('content')
    <section>
        <div class="card">
            <div class="card-content collapse show">
                <div class="card-body card-dashboard">
                    <div class="card-body card-dashboard">
                        <form role="form"
                              action="{{ route('orders.update', $order->id) }}"
                              method="POST"
                              class="form"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="form-group col-md-4 {{ $errors->has('ordered_time') ? ' error' : '' }}">
                                    <label for="start_time">Order time</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="ordered_time" id="start_time"
                                               placeholder="Please Enter Match Start Time"
                                               value="{{ isset($order) ? $order->ordered_time : old('ordered_time') }}"
                                               autocomplete="off" required/>
                                    </div>
                                    <div class="help-block"></div>
                                    @if ($errors->has('ordered_time'))
                                        <div class="help-block">{{ $errors->first('ordered_time') }}</div>
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="eventInput3">Factory Name</label>
                                        <select name="factory_name" class="form-control">
                                            @foreach($factories as $factory)
                                                <option value="{{$factory}}" {{ isset($order) && $order->factory_name == $factory ? 'selected' : "" }}>{{ $factory }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('worker_id') ? ' error' : '' }}">
                                    <label for="worker_id">Worker Name</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="worker_id" id="worker_id"
                                               placeholder="Enter Worker Id"
                                               value="{{ isset($order) ? $order->worker_id : old('worker_id') }}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('worker_name') ? ' error' : '' }}">
                                    <label for="worker_name">Worker Name</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="worker_name" id="worker_name"
                                               placeholder="Enter Worker Name"
                                               value="{{ isset($order) ? $order->worker_name : old('worker_name') }}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('worker_phone') ? ' error' : '' }}">
                                    <label for="worker_phone">Worker Phone</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="worker_phone" id="worker_phone"
                                               placeholder="Enter Worker Phone"
                                               value="{{ isset($order) ? $order->worker_phone : old('worker_phone') }}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>

                                <div class="form-group col-md-4 {{ $errors->has('supervisor_id') ? ' error' : '' }}">
                                    <label for="supervisor_id">Supervisor Id</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="supervisor_id" id="supervisor_id"
                                               placeholder="Enter Supervisor Name"
                                               value="{{ isset($order) ? $order->supervisor_id : old('supervisor_id') }}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('supervisor_name') ? ' error' : '' }}">
                                    <label for="supervisor_name">Supervisor Name</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="supervisor_name" id="supervisor_name"
                                               placeholder="Enter Supervisor Name"
                                               value="{{ isset($order) ? $order->supervisor_name : old('supervisor_name') }}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('25_kg_rice_bag_28') ? ' error' : '' }}">
                                    <label for="25_kg_rice_bag_28">25 Kg Rice Bag (28)</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="25_kg_rice_bag_28" id="25_kg_rice_bag_28"
                                               placeholder="Enter Number of bags"
                                               value="{{ isset($order) ? $order['25_kg_rice_bag_28'] : old('25_kg_rice_bag_28')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('25_kg_rice_bag_28_unit_price') ? ' error' : '' }}">
                                    <label for="25_kg_rice_bag_28_unit_price">25 Kg Rice Bag(28) Unit Price</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="25_kg_rice_bag_28_unit_price" id="25_kg_rice_bag_28_unit_price"
                                               placeholder="Enter Unit Price"
                                               value="{{ isset($order) ? $order['25_kg_rice_bag_28_unit_price'] : old('25_kg_rice_bag_28_unit_price')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('25_kg_rice_bag_29') ? ' error' : '' }}">
                                    <label for="25_kg_rice_bag_29">25 Kg Rice Bag (29)</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="25_kg_rice_bag_29" id="25_kg_rice_bag_29"
                                               placeholder="Enter Number of bags"
                                               value="{{ isset($order) ? $order['25_kg_rice_bag_29'] : old('25_kg_rice_bag_29')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('25_kg_rice_bag_29_unit_price') ? ' error' : '' }}">
                                    <label for="25_kg_rice_bag_29_unit_price">25 Kg Rice Bag(29) Unit Price</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="25_kg_rice_bag_29_unit_price" id="25_kg_rice_bag_29_unit_price"
                                               placeholder="Enter Unit Price"
                                               value="{{ isset($order) ? $order['25_kg_rice_bag_29_unit_price'] : old('25_kg_rice_bag_29_unit_price')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('oil_2_liter') ? ' error' : '' }}">
                                    <label for="oil_2_liter">Oil 2 Liter</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="oil_2_liter" id="oil_2_liter"
                                               placeholder="Enter Oil Amount"
                                               value="{{ isset($order) ? $order->oil_2_liter : old('oil_2_liter')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('oil_2_liter_unit_price') ? ' error' : '' }}">
                                    <label for="oil_2_liter_unit_price">Oil 2 Liter Unit Price</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="oil_2_liter_unit_price" id="oil_2_liter_unit_price"
                                               placeholder="Enter Unit Price"
                                               value="{{ isset($order) ? $order->oil_2_liter_unit_price : old('oil_2_liter_unit_price')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('oil_5_liter') ? ' error' : '' }}">
                                    <label for="oil_5_liter">Oil 5 Liter</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="oil_5_liter" id="oil_5_liter"
                                               placeholder="Enter Oil Amount"
                                               value="{{ isset($order) ? $order->oil_5_liter : old('oil_5_liter')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('oil_5_liter_unit_price') ? ' error' : '' }}">
                                    <label for="oil_5_liter_unit_price">Oil 5 Liter Unit Price</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="oil_5_liter_unit_price" id="oil_5_liter_unit_price"
                                               placeholder="Enter Unit Price"
                                               value="{{ isset($order) ? $order->oil_5_liter_unit_price : old('oil_5_liter_unit_price')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('Lentils') ? ' error' : '' }}">
                                    <label for="Lentils">Lentils</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="Lentils" id="Lentils"
                                               placeholder="Enter Lentils Amount(KG)"
                                               value="{{ isset($order) ? $order->Lentils : old('Lentils')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('Lentils_unit_price') ? ' error' : '' }}">
                                    <label for="Lentils_unit_price">Lentils Unit Price</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="Lentils_unit_price" id="Lentils_unit_price"
                                               placeholder="Enter Unit Price"
                                               value="{{ isset($order) ? $order->Lentils_unit_price : old('Lentils_unit_price')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('flour') ? ' error' : '' }}">
                                    <label for="Lentils">Flour</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="flour" id="flour"
                                               placeholder="Enter Flour Amount(KG)"
                                               value="{{ isset($order) ? $order->flour : old('flour')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('flour_unit_price') ? ' error' : '' }}">
                                    <label for="flour_unit_price">Flour Unit Price</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="flour_unit_price" id="flour_unit_price"
                                               placeholder="Enter Unit Price"
                                               value="{{ isset($order) ? $order->flour_unit_price : old('flour_unit_price')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('potatoes') ? ' error' : '' }}">
                                    <label for="potatoes">Potatoes</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="potatoes" id="potatoes"
                                               placeholder="Enter Potatoes Amount(KG)"
                                               value="{{ isset($order) ? $order->potatoes : old('potatoes')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('potatoes_unit_price') ? ' error' : '' }}">
                                    <label for="potatoes_unit_price">Potatoes Unit Price</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="potatoes_unit_price" id="potatoes_unit_price"
                                               placeholder="Enter Unit Price"
                                               value="{{ isset($order) ? $order->potatoes_unit_price : old('potatoes_unit_price')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('onion') ? ' error' : '' }}">
                                    <label for="onion">Onion</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="onion" id="onion"
                                               placeholder="Enter Onion Amount(KG)"
                                               value="{{ isset($order) ? $order->onion : old('onion')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('onion_unit_price') ? ' error' : '' }}">
                                    <label for="onion_unit_price">Onion Unit Price</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="onion_unit_price" id="onion_unit_price"
                                               placeholder="Enter Unit Price"
                                               value="{{ isset($order) ? $order->onion_unit_price : old('onion_unit_price')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('salt') ? ' error' : '' }}">
                                    <label for="salt">Salt</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="salt" id="salt"
                                               placeholder="Enter Salt Amount(KG)"
                                               value="{{ isset($order) ? $order->salt : old('salt')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('salt_unit_price') ? ' error' : '' }}">
                                    <label for="salt_unit_price">Salt Unit Price</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="salt_unit_price" id="salt_unit_price"
                                               placeholder="Enter Unit Price"
                                               value="{{ isset($order) ? $order->salt_unit_price : old('salt_unit_price')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('sugar') ? ' error' : '' }}">
                                    <label for="sugar">Sugar</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="sugar" id="sugar"
                                               placeholder="Enter Sugar Amount(KG)"
                                               value="{{ isset($order) ? $order->sugar : old('sugar')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('sugar_unit_price') ? ' error' : '' }}">
                                    <label for="sugar_unit_price">Sugar Unit Price</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="sugar_unit_price" id="sugar_unit_price"
                                               placeholder="Enter Unit Price"
                                               value="{{ isset($order) ? $order->sugar_unit_price : old('sugar_unit_price')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 {{ $errors->has('total') ? ' error' : '' }}">
                                    <label for="total">Total Amount</label>
                                    <div class='input-group'>
                                        <input type='text' class="form-control" name="total" id="total"
                                               placeholder="Enter Total Amount"
                                               value="{{ isset($order) ? $order->total : old('total')}}"
                                               autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="eventInput3">Status</label>
                                        <select name="status" class="form-control">
                                            <option value="1" {{ $order->status == 1 ? 'selected' : '' }}>Complete</option>
                                            <option value="0" {{ $order->status == 0 ? 'selected' : '' }}>Incomplete</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success mt-2">
                                    <i class="ft-save"></i> Update
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop

@push('page-css')
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/css/plugins/forms/validation/form-validation.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/vendors/js/pickers/dateTime/css/bootstrap-datetimepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/css/forms/selects/select2.min.css') }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/editors/summernote.css') }}">
@endpush

@push('page-js')
    <script src="{{ asset('theme/vendors/js/pickers/dateTime/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('theme/vendors/js/pickers/dateTime/bootstrap-datetimepicker.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $(".product-list").select2()
            $('.report-repeater').repeater();

            let startTime = $('#start_time');

            function dateTime(element){
                var date = new Date();
                date.setDate(date.getDate());
                element.datetimepicker({
                    format : 'YYYY-MM-DD HH:mm:ss',
                    showClose: true,
                });
            }

            dateTime(startTime)
        });
    </script>
@endpush
