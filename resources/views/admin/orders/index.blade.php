@extends('layouts.admin')
@section('title', 'Order List')
@section('card_name', 'Order List')
@section('breadcrumb')
    <li class="breadcrumb-item active">Order List</li>
@endsection

{{--@section('action')--}}
{{--        <a href="{{route('orders.create')}}" class="btn btn-primary round btn-glow px-2"><i--}}
{{--                class="la la-plus"></i>--}}
{{--            Create Order--}}
{{--        </a>--}}
{{--@endsection--}}

@section('content')
    <section>
        <div class="card">
            <div class="card-content collapse show">
                <div class="card-body card-dashboard">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form" method="POST" id="uploadProduct" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="message">Upload Excel</label> <a href="{{ asset('sample-format/order_format.xlsx')}}" class="text-info ml-2">Download Sample Format</a></br>
                                    </div>
                                    <p class="text-left">
                                        <small class="warning text-muted">
                                            Please download the format and upload in a specific format.
                                        </small>
                                    </p>
                                    <input type="file" class="dropify" name="product_file" data-height="80"
                                           data-allowed-file-extensions="xlsx" required/>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group float-right" style="margin-top:15px;">
                                        <button class="btn btn-success" style="width:100%;padding:7.5px 12px"
                                                type="submit">Submit
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="card">
            <div class="card-content collapse show">
                <div class="card-body card-dashboard">
                    <div class="row">
                        <div class="col-md-12">
                            <form role="form"
                                  action="{{ route('orders.search') }}"
                                  method="POST"
                                  class="form"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="title" class="required">Supervisor Id</label>
                                        <input class="form-control"
                                               name="supervisor_id"
                                               id="supervisor_id"
                                               value="{{ isset($searchItem['supervisor_id']) && $searchItem['supervisor_id']!=null ?  $searchItem['supervisor_id']  : ""}}"
                                               placeholder="Enter Supervisor Id"
                                               >
                                        @if($errors->has('supervisor_id'))
                                            <p class="text-left">
                                                <small class="danger text-muted">{{ $errors->first('supervisor_id') }}</small>
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="title" class="required">Worker Id</label>
                                        <input class="form-control"
                                               name="worker_id"
                                               id="worker_id"
                                               value="{{ isset($searchItem['worker_id']) && $searchItem['worker_id']!=null ?  $searchItem['worker_id']  : ""}}"
                                               placeholder="Enter Worker Id"
                                               >
                                        @if($errors->has('worker_id'))
                                            <p class="text-left">
                                                <small class="danger text-muted">{{ $errors->first('worker_id') }}</small>
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="title" class="required">Worker Phone</label>
                                        <input class="form-control"
                                               name="worker_phone"
                                               id="worker_phone"
                                               value="{{ isset($searchItem['worker_phone']) && $searchItem['worker_phone']!=null ?  $searchItem['worker_phone']  : ""}}"
                                               placeholder="Enter Worker Phone"
                                               >
                                        @if($errors->has('worker_phone'))
                                            <p class="text-left">
                                                <small class="danger text-muted">{{ $errors->first('worker_phone') }}</small>
                                            </p>
                                        @endif
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="eventInput3">Factory Name</label>
                                            <select name="factory_name" class="form-control">
                                                <option value="" >Select Factory Name</option>
                                                @foreach($factories as $factory)
                                                    <option value="{{$factory}}" {{ isset($searchItem['factory_name']) && $searchItem['factory_name'] == $factory ?  'selected'  : ""}} >{{ $factory }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="eventInput3">Factory Name</label>
                                            <select name="status" class="form-control">
                                                 <option value="" >Select Status</option>
                                                    <option value="1" {{ isset($searchItem['status']) && $searchItem['status'] == 1 ?  'selected'  : ""}} >Complete</option>
                                                    <option value="0" {{ isset($searchItem['status']) && $searchItem['status'] == 0 ?  'selected'  : ""}}>Incomplete</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-outline-warning mt-2">
                                        <i class="ft-search"></i> Search
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="card card-info mt-0" style="box-shadow: 0px 0px">
            <div class="card-content">
                <div class="card-body card-dashboard">
                    <table class="table table-striped table-bordered alt-pagination no-footer dataTable" id="Example1"
                           role="grid" aria-describedby="Example1_info" style="">
                        <thead>
                        <tr>
                            <th>SL</th>
                            <th>Order Time</th>
                            <th>Factory Name</th>
                            <th>Supervisor Id</th>
                            <th>Worker Name</th>
                            <th>Worker Id</th>
                            <th>Worker Phone</th>
                            <th>Total</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($orders as $data)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$data->ordered_time}}</td>
                                <td>{{$data->factory_name}}</td>
                                <td>{{$data->supervisor_id}}</td>
                                <td>{{$data->worker_name}}</td>
                                <td>{{$data->worker_id}}</td>
                                <td>{{$data->worker_phone}}</td>
                                <td>{{$data->total}}</td>
                                <td>
                                    @if(isset($data->status) && $data->status == 1)
                                        <a href="{{ route('order.status', [$data->id]) }}" role="button" class="btn-sm btn-outline-success border-0"><i class="la la-complete" aria-hidden="true">Complete</i></a>
                                    @else
                                        <a href="{{ route('order.status', [$data->id]) }}" role="button" class="btn-sm btn-outline-warning border-0"><i class="la la-Incomplete" aria-hidden="true">Incomplete</i></a>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('orders.edit', [$data->id]) }}" role="button" class="btn-sm btn-outline-info border-0"><i class="la la-pencil" aria-hidden="true"></i></a>
                                    @if($userType == 'agroshift')
                                        <a href="{{ route('order.delete', [$data->id]) }}" role="button" class="btn-sm btn-outline-danger border-0"><i class="la la-trash" aria-hidden="true"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </section>

@endsection

@push('style')
    <link rel="stylesheet" href="{{asset('plugins')}}/sweetalert2/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css"
          href="{{asset('app-assets')}}/vendors/css/tables/datatable/datatables.min.css">
    <style>
        table.dataTable tbody td {
            max-height: 40px;
        }
    </style>
@endpush
@push('page-js')
    <script src="{{asset('plugins')}}/sweetalert2/sweetalert2.min.js"></script>
    <script src="{{asset('app-assets')}}/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="{{asset('app-assets')}}/vendors/js/tables/datatable/dataTables.buttons.min.js"
            type="text/javascript"></script>
    <script src="{{asset('app-assets')}}/js/scripts/tables/datatables/datatable-advanced.js"
            type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $('#Example1').DataTable({
                buttons: [],
                paging: true,
                searching: true,
                "bDestroy": true,
            });

            $('.delete').click(function () {
                var id = $(this).attr('data-id');

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    html: jQuery('.delete_btn').html(),
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        event.preventDefault();
                        document.getElementById(`delete-form-${id}`).submit();
                    }
                })
            })
            $('.status').click(function () {
                var id = $(this).attr('data-id');

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    html: jQuery('.status_btn').html(),
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Change Status!'
                }).then((result) => {
                    if (result.value) {
                        event.preventDefault();
                        document.getElementById(`delete-form-${id}`).submit();
                    }
                })
            })
            $('.dropify').dropify({
                messages: {
                    'default': 'Browse for an Excel File to upload',
                    'replace': 'Click to replace',
                    'remove': 'Remove',
                    'error': 'Choose correct file format'
                }
            });
            $('#uploadProduct').submit(function (e) {
                e.preventDefault();

                swal.fire({
                    title: 'Data Uploading.Please Wait ...',
                    allowEscapeKey: false,
                    allowOutsideClick: false,
                    onOpen: () => {
                        swal.showLoading();
                    }
                });

                let formData = new FormData($(this)[0]);

                $.ajax({
                    url: '{{ route('order-excel-upload')}}',
                    type: 'POST',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: formData,
                    success: function (result) {
                        if (result.success) {
                            swal.fire({
                                title: 'Excel Upload Successfully!',
                                type: 'success',
                                timer: 2000,
                                showConfirmButton: false
                            });
                        } else {
                            swal.close();
                            swal.fire({
                                title: result.message,
                                type: 'error',
                            });
                        }
                        $(".dropify-clear").trigger("click");

                    },
                    error: function (data) {
                        swal.fire({
                            title: 'Failed to upload excel',
                            type: 'error',
                        });
                    }
                });

            });
        });
    </script>
@endpush
