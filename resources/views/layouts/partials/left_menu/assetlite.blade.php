{{--------------------------------------------------------------------------------------------------------------------}}
{{---------------------------------------------------------Asset Lite-------------------------------------------------}}
{{--------------------------------------------------------------------------------------------------------------------}}
@if(Auth::user()->type == 'agroshift')
    <li class="nav-item">
        <a href="#">
            <i class="la la la-cogs"></i><span class="menu-title" data-i18n="nav.templates.main">Settings & Others</span>
        </a>
        <ul class="menu-content">
            <li class="nav-item">
                <a href="#"><i class="la la-users"></i>
                    <span class="menu-title" data-i18n="nav.templates.main">User Management</span></a>
                <ul class="menu-content">
                        <li class="{{ is_active_url('authorize/users')}}">
                            <a class="menu-item" href="{{ url('authorize/users') }}"
                               data-i18n="nav.templates.vert.classic_menu"><i
                                    class="la la-user"></i> User</a>
                        </li>
                        <li class="{{ is_active_url('authorize/roles')}}">
                            <a class="menu-item" href="{{ url('authorize/roles') }}"
                               data-i18n="nav.templates.vert.classic_menu"><i
                                    class="la la-cubes"></i> Role</a>
                        </li>
                        <li class="{{ is_active_url('access-logs')}}">
                            <a class="menu-item" href="{{ url('access-logs') }}"
                               data-i18n="nav.templates.vert.classic_menu"><i
                                    class="la la-lock"></i> Access Logs</a>
                        </li>
                </ul>
            </li>
        </ul>
    </li>
@endif
<li class="{{ is_active_url('orders')}}">
    <a class="menu-item" href="{{ url('orders') }}"
       data-i18n="nav.templates.vert.classic_menu"><i
            class="la la-file"></i>Order</a>
</li>
{{--------------------------------------------------------------------------------------------------------------------}}
{{---------------------------------------------------------Asset Lite End---------------------------------------------}}
{{--------------------------------------------------------------------------------------------------------------------}}
