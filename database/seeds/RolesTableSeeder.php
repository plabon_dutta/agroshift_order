<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use Carbon\Carbon;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS =0;");
        DB::table('roles')->truncate();
        $roles = [
            [
                'name' => 'Agroshift Admin',
                'alias' => 'admin',
                'user_type' => 'admin',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'name' => 'Moderator',
                'alias' => 'moderator',
                'user_type' => 'moderator',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
        ];

        foreach ($roles as $role) {
            Role::create($role);
        }
    }
}
