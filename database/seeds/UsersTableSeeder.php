<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_user')->truncate();
        DB::statement("SET FOREIGN_KEY_CHECKS =0;");
        User::truncate();

        $myblUsers = [
            [
                'name' => 'Agroshift Admin',
                'email' => 'agroshift-admin@admin.com',
                'type' => 'agroshift',
                'phone' => '0191911111541',
                'uid' => uniqid(),
                'password' => Hash::make('123456'),
            ]
        ];

        for ($i = 0; $i < count($myblUsers); $i++) {
            DB::table('users')->insert($myblUsers[$i]);
            DB::table('role_user')->insert(
                [
                'role_id' => 1,
                'user_id' => $i + 1
                ]
            );
        }
    }
}
