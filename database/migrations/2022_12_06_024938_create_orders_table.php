<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ordered_time')->nullable();
            $table->string('factory_name')->nullable();
            $table->string('worker_name')->nullable();
            $table->string('worker_id')->nullable();
            $table->string('worker_phone')->nullable();
            $table->string('supervisor_name')->nullable();
            $table->string('supervisor_id')->nullable();
            $table->string('25_kg_rice_bag_28')->nullable();
            $table->double('25_kg_rice_bag_28_unit_price', 8, 2)->nullable();
            $table->string('25_kg_rice_bag_29')->nullable();
            $table->double('25_kg_rice_bag_29_unit_price', 8, 2)->nullable();
            $table->string('oil_2_liter')->nullable();
            $table->double('oil_2_liter_unit_price', 8, 2)->nullable();
            $table->string('oil_5_liter')->nullable();
            $table->double('oil_5_liter_unit_price', 8, 2)->nullable();
            $table->string('Lentils')->nullable();
            $table->double('Lentils_unit_price', 8, 2)->nullable();
            $table->string('flour')->nullable();
            $table->double('flour_unit_price', 8, 2)->nullable();
            $table->string('potatoes')->nullable();
            $table->double('potatoes_unit_price', 8, 2)->nullable();
            $table->string('onion')->nullable();
            $table->double('onion_unit_price', 8, 2)->nullable();
            $table->string('salt')->nullable();
            $table->double('salt_unit_price', 8, 2)->nullable();
            $table->string('sugar')->nullable();
            $table->double('sugar_unit_price', 8, 2)->nullable();
            $table->double('total', 8, 2)->nullable();
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
