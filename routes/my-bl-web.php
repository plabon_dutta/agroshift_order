<?php

//$serve = "mysql-5";
//$user = "root";
//$password = "root";
//$db = "bl_cms";
//try {
//    $con = mysqli_connect($serve, $user, $password, $db);
//
//    $query = ' Select id, referrer from cs_selfcare_referrers where code_type = "retailer" AND is_active = 1';
//    $result = $con->query($query);
//
//    foreach ($result as $value) {
//        $msisdn = substr($value->referrer->msisdn, -9);
//        $newCode = decoct($msisdn);
//        $updateQuery = "update cs_selfcare_referrers set referral_code = '$newCode' where id = value->id";
//        $con->query($updateQuery);
//    }
//}catch (PDOException $e){
//
//    echo $sql . "<br>" . $e->getMessage();
//}

/*
|--------------------------------------------------------------------------
| Web Routes for MY BL
|--------------------------------------------------------------------------
|
| Here is where you can register web Routes for your application. These
| Routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

use App\Services\NewCampaignModality\MyBlCampaignWinnerSelectionService;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['CheckFistLogin']], function () {
    Route::get('orders', 'OrderController@index')->name('orders.index');
    Route::get('orders-edit/{id}', 'OrderController@edit')->name('orders.edit');
    Route::put('orders-update/{id}', 'OrderController@update')->name('orders.update');
    Route::get('orders-create', 'OrderController@create')->name('orders.create');
    Route::get('orders/destroy/{id}', 'OrderController@destroy')->name('order.delete');
    Route::get('orders/change/{id}', 'OrderController@statusChange')->name('order.status');
    Route::post('orders-search', 'OrderController@search')->name('orders.search');
    Route::post('order-excel-upload', 'OrderController@excelUpload')->name('order-excel-upload');

    //Route::get('orders-excel-upload', 'OrderController@uploadExcel')->name('excel.upload');
    //Route::post('orders-excel-store', 'OrderController@uploadProductCodeAndSlugByExcel')->name('al-product-category-sync');
});
