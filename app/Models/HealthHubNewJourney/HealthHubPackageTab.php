<?php

namespace App\Models\HealthHubNewJourney;

use Illuminate\Database\Eloquent\Model;

class HealthHubPackageTab extends Model
{
    protected $fillable = ['package_id', 'package_category_id'];
}
