<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PopupBanner extends Model
{
    //

    protected $fillable = ['banner','deeplink','status','is_priority'];
}
