<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateMyblProductRequest;
use App\Models\MyBlInternetOffersCategory;
use App\Models\MyBlProduct;
use App\Repositories\MyBlProductRepository;
use App\Repositories\MyBlProductSchedulerRepository;
use App\Services\BaseMsisdnService;
use App\Services\FreeProductPurchaseReportService;
use App\Services\MyBlProductSchedulerService;
use App\Services\OrderService;
use App\Services\ProductCoreService;
use App\Services\ProductTagService;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class OrderController extends Controller
{
    protected $orderService;
    public function __construct(OrderService $orderService) {
        $this->orderService = $orderService;
    }

    public function index()
    {
        $userType = Auth::user()->type;
        $factories = $this->orderService->findFactories();
        $orders = $this->orderService->findAll();
        return view('admin.orders.index', compact('orders', 'factories', 'userType'));
    }

    public function create()
    {

    }
    public function edit($id)
    {
        $factories = $this->orderService->findFactories();
        $order = $this->orderService->findOne($id);
//        dd($order['25_kg_rice_bag_28']);
        return view('admin.orders.edit', compact('order', 'factories'));
    }

    public function update(Request $request, $id) {

        $flag = $this->orderService->update($request->all(), $id);

        if($flag) {
            Session::flash('success', 'Order Update successfully');
        } else {
            Session::flash('danger', 'Order Update Failed');
        }
        return redirect('orders');
    }

    public function destroy($id)
    {
        $order = $this->orderService->findOne($id);
        $order->delete();

        Session::flash('success', 'Order Deleted successful');
        return redirect('orders');
    }

    public function excelUpload(Request  $request)
    {
        try {
            $file = $request->file('product_file');
            $path = $file->storeAs(
                'al-products/' . strtotime(now() . '/'),
                "products" . '.' . $file->getClientOriginalExtension(),
                'public'
            );
            $path = Storage::disk('public')->path($path);

            return $this->orderService->saveOrder($path);

        } catch (\Exception $e) {
            if($e->getMessage() == 'Undefined offset: 9'){
                return response()->json([
                    'success' => 'SUCCESS'
                ], 200);
            } else {
                $response = [
                    'success' => 'FAILED',
                    'errors' => $e->getMessage()
                ];
                return response()->json($response, 500);
            }
        }
    }

    public function statusChange($id)
    {
        $order = $this->orderService->findOne($id);
        $order->update(['status' => !$order->status]);

        return redirect('orders');
    }

    public function search(Request $request)
    {
        $factories = $this->orderService->findFactories();
        $orders = $this->orderService->findAll();
        $userType = Auth::user()->type;
        if(isset($request->supervisor_id) && $request->supervisor_id != null){
            $orders = $orders->where('supervisor_id', $request->supervisor_id);
        }
        if(isset($request->worker_id) && $request->worker_id != null){
            $orders = $orders->where('worker_id', $request->worker_id);
        }
        if(isset($request->worker_phone) && $request->worker_phone != null){
            $orders = $orders->where('worker_phone', $request->worker_phone);
        }
        if(isset($request->factory_name) && $request->factory_name != null){
            $orders = $orders->where('factory_name', $request->factory_name);
        }
        if(isset($request->status) && $request->status != null){
            $orders = $orders->where('status', $request->status);
        }
        $searchItem = $request->all();

        return view('admin.orders.index', compact('orders', 'factories', 'searchItem', 'userType'));
    }
//    public function searchProductCodes(Request $request)
//    {
//        $search_term = $request->term;
//        $codes = $this->service->searchProductCodes($search_term)->pluck('product_code');
//
//        $data = [];
//
//        foreach ($codes as $code) {
//            $data ['results'][] = [
//                'id' => $code,
//                'text' => $code,
//            ];
//        }
//
//        return $data;
//    }
//
//    /**
//     * @param $product_code
//     * @return Factory|View
//     */
//    public function getProductDetails($product_code)
//    {
//        $orders = Orders::where('product_code', $product_code)->first();
//
//        return view(
//            'admin.my-bl-products.product-details',
//            compact('details', 'internet_categories', 'tags', 'disablePinToTop', 'baseMsisdnGroups', 'productSchedulerData', 'productScheduleRunning', 'warningText')
//        );
//    }
//
//    /**
//     * @return Factory|View
//     */
//    public function index()
//    {
//        return view('admin.my-bl-products.mybl_product_entry');
//    }
//
//    public function inactiveProducts(Request $request)
//    {
//        $inactiveProducts = $this->service->getInactiveProducts();
//        return view('admin.my-bl-products.inactive_products', compact('inactiveProducts'));
//    }
//
//    public function activateProduct($productCode)
//    {
//        if ($this->service->activateProduct($productCode)) {
//            Session::flash('success', 'Product activated! Please find the product in product list');
//        } else {
//            Session::flash('success', 'Error while activating! Please retry');
//        }
//
//        return redirect()->back();
//    }
//    /**
//     * @return Factory|View
//     */
//    public function create()
//    {
//        $tags = $this->productTagService
//            ->findAll(null, null, ['column' => 'priority', 'direction' => 'asc'])
//            ->pluck('title', 'id');
//        $internet_categories = MyBlInternetOffersCategory::where('platform', 'mybl')->pluck('name', 'id')->sortBy('sort');
//
//        $pinToTopCount = MyBlProduct::where('pin_to_top', 1)->where('status', 1)->count();
//        $disablePinToTop = (($pinToTopCount >= config('productMapping.mybl.max_no_of_pin_to_top')));
//        $baseMsisdnGroups = $this->baseMsisdnService->findAll();
//
//        return view(
//            'admin.my-bl-products.create-product',
//            compact(
//                'tags',
//                'internet_categories',
//                'disablePinToTop',
//                'baseMsisdnGroups'
//            )
//        );
//    }
//
//    public function store(UpdateMyblProductRequest $request)
//    {
//        return $this->service->storeMyblProducts($request);
//    }
//
//    /**
//     * @param Request $request
//     * @return JsonResponse
//     */
//    public function uploadProductByExcel(Request $request)
//    {
//        try {
//            $file = $request->file('product_file');
//            $path = $file->storeAs(
//                'products/' . strtotime(now() . '/'),
//                "products" . '.' . $file->getClientOriginalExtension(),
//                'public'
//            );
//
//            $path = Storage::disk('public')->path($path);
//
//            $this->service->mapMyBlProduct($path);
//
//            // reset product keys from redis
//            /**
//             * Commenting reset redis key code according to BL requirement on 24 June 2021
//             */
//            //$this->service->resetProductRedisKeys();
//            $this->service->syncSearch();
//
//            $response = [
//                'success' => 'SUCCESS'
//            ];
//            return response()->json($response, 200);
//        } catch (\Exception $e) {
//            $response = [
//                'success' => 'FAILED',
//                'errors' => $e->getMessage()
//            ];
//            return response()->json($response, 500);
//        }
//    }
//
//    /**
//     * @param Request $request
//     * @return array
//     */
//    public function getMyblProducts(Request $request)
//    {
//        return $this->service->getMyblProducts($request);
//    }
//
//    /**
//     * @param UpdateMyblProductRequest $request
//     * @param $product_code
//     * @return RedirectResponse
//     * @throws \Exception
//     */
//    public function updateMyblProducts(UpdateMyblProductRequest $request, $product_code)
//    {
//        return $this->service->updateMyblProducts($request, $product_code);
//    }
//
//    /**
//     * @throws \Box\Spout\Common\Exception\IOException
//     * @throws \Box\Spout\Writer\Exception\WriterNotOpenedException
//     */
//    public function downloadMyblProducts()
//    {
//        return $this->service->downloadMyblProducts();
//    }
//
//    public function resetRedisProductKey()
//    {
//        //dd(in_array(\Carbon\Carbon::now()->format('H'), [0, 1, 2, 3]));
//        $this->service->resetProductRedisKeys();
//        return redirect()->back()->with('success', 'Redis key reset is successful!');
//    }
//
//    public function imageRemove($id)
//    {
//        return $this->service->imgRemove($id);
//    }
//
//    public function freeProductPurchaseReport(Request $request)
//    {
//        $purchasedProducts = $this->freeProductPurchaseReportService->analyticsData($request->all());
//        return view('admin.free-product-analytic.purchase-product', compact('purchasedProducts'));
//    }
//
//    public function purchaseDetails(Request $request, $purchaseProductId)
//    {
//        if ($request->ajax()) {
//            return $this->freeProductPurchaseReportService->msisdnPurchaseDetails($request, $purchaseProductId);
//        }
//        $purchaseProduct = $this->freeProductPurchaseReportService->findOne($purchaseProductId);
//        return view('admin.free-product-analytic.purchase-msisdn', compact('purchaseProduct'));
//    }
//
//    public function getScheduleProduct()
//    {
//        $currentTime = Carbon::parse()->format('Y-m-d H:i:s');
//        $scheduleProducts = $this->myblProductScheduleRepository->getAllScheduleProducts();
//
//        return view('admin.my-bl-products.schedule-products', compact('scheduleProducts', 'currentTime'));
//    }
//
//    public function getScheduleProductRevert($id)
//    {
//
//        return $this->myBlProductSchedulerService->cancelSchedule($id);
//    }
//
//    public function scheduleProductsView($id)
//    {
//        $scheduleProduct = $this->myblProductScheduleRepository->findOne($id);
//
//        $productCode = $scheduleProduct['product_code'];
//        $product = $this->myblProductRepository->findByProperties(['product_code' => $productCode], ['media', 'show_in_home', 'pin_to_top', 'base_msisdn_group_id', 'tag', 'is_visible']);
//        $product = $product->first();
//
//        $tagTitleForScheduler = null;
//        $baseMsisdnTitleForSchedule = null;
//        $baseMsisdnTitleForProduct = null;
//
//        if(!is_null($scheduleProduct['tags'])) {
//            $tagIds = json_decode($scheduleProduct['tags']);
//            $tag = $this->myBlProductSchedulerService->getTag($tagIds[0]);
//            $tagTitleForScheduler = $tag->title;
//        }
//
//        if(!is_null($scheduleProduct['base_msisdn_group_id'])) {
//            $baseMsisdnTitleForSchedule = $this->baseMsisdnService->getMsisdnGroupTitle($scheduleProduct['base_msisdn_group_id']);
//        }
//
//
//        if(!is_null($product['base_msisdn_group_id'])) {
//
//            $baseMsisdnTitleForProduct = $this->baseMsisdnService->getMsisdnGroupTitle($product['base_msisdn_group_id']);
//        }
//
//        $productScheduleRunning = false;
//
//        $currentTime = Carbon::parse()->format('Y-m-d H:i:s');
//        if (($currentTime >= $scheduleProduct->start_date && $currentTime <= $scheduleProduct->end_date && $scheduleProduct->change_state_status)) {
//            $productScheduleRunning = true;
//        }
//
//        return view('admin.my-bl-products.schedule-product-view', compact('scheduleProduct', 'product', 'tagTitleForScheduler', 'baseMsisdnTitleForSchedule', 'baseMsisdnTitleForProduct', 'productScheduleRunning'));
//    }
}
