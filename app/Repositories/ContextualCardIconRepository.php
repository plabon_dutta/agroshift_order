<?php


namespace App\Repositories;
use App\Models\ContextualCardIcon;

class ContextualCardIconRepository  extends BaseRepository
{
    public $modelName = ContextualCardIcon::class;
}
