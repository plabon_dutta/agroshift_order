<?php

namespace App\Repositories;

use App\Order;


class OrderRepository extends BaseRepository
{
    public $modelName = Order::class;

    public function findAllOrderBY(){
        return $this->model::orderBy('ordered_time', 'desc')->get();
    }

    public function findFactories()
    {
        return ($this->model::distinct('factory_name')->pluck('factory_name'));
    }
}
