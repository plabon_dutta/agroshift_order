<?php

namespace App\Services;

use App\Models\MyBlProduct;
use App\Repositories\OrderRepository;
use App\Services\BlApiHub\BaseService;
use Box\Spout\Common\Type;
use Box\Spout\Reader\Common\Creator\ReaderFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderService extends BaseService
{
    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function findAll()
    {
        return $this->orderRepository->findAllOrderBY();
    }

    public function findFactories()
    {
        return $this->orderRepository->findFactories();
    }

    public function saveOrder($path)
    {
        try {
            $productCodes = [];
            $slugs       = [];
            $reader = ReaderFactory::createFromType(Type::XLSX); // for XLSX files
            $file_path = $path;
            $reader->open($file_path);

            foreach ($reader->getSheetIterator() as $sheet) {
                $row_number = 1;
                foreach ($sheet->getRowIterator() as $row) {
                    if($row_number==1){
                        ++$row_number;
                        continue;
                    }
                    $cells = $row->getCells();
                    $data['ordered_time'] = (array)($cells[0]->getValue());
                    $data['ordered_time'] = $data['ordered_time']['date'] ?? "";
                    $data['factory_name']     = ($cells[1]->getValue()) ?? "";
                    $data['worker_name']    = ($cells[2]->getValue()) ?? "";
                    $data['worker_id']   = ($cells[3]->getValue()) ?? "";
                    $data['worker_phone']    = ($cells[4]->getValue()) ?? "";
                    $data['supervisor_name']    = ($cells[5]->getValue()) ?? "";
                    $data['supervisor_id']    = ($cells[6]->getValue()) ?? "";
                    $data['25_kg_rice_bag_28']   = ($cells[7]->getValue()) ?? "";
                    $data['25_kg_rice_bag_29']   = ($cells[8]->getValue()) ?? "";
                    $data['oil_2_liter']   = ($cells[9]->getValue()) ?? "";
                    $data['oil_5_liter']   = ($cells[10]->getValue()) ?? "";
                    $data['Lentils']   = ($cells[11]->getValue()) ?? "";
                    $data['flour']   = ($cells[12]->getValue()) ?? "";
                    $data['potatoes']   = ($cells[13]->getValue()) ?? "";
                    $data['onion']   = ($cells[14]->getValue()) ?? "";
                    $data['salt']   = ($cells[15]->getValue()) ?? "";
                    $data['sugar']   = ($cells[16]->getValue()) ?? "";
                    $data['25_kg_rice_bag_28_unit_price']   = ($cells[17]->getValue()) ?? "";
                    $data['25_kg_rice_bag_29_unit_price']   = ($cells[18]->getValue()) ?? "";
                    $data['oil_2_liter_unit_price']   = ($cells[19]->getValue()) ?? "";
                    $data['oil_5_liter_unit_price']   = ($cells[20]->getValue()) ?? "";
                    $data['Lentils_unit_price']   = ($cells[21]->getValue()) ?? "";
                    $data['flour_unit_price']   = ($cells[22]->getValue()) ?? "";
                    $data['potatoes_unit_price']   = ($cells[23]->getValue()) ?? "";
                    $data['onion_unit_price']   = ($cells[24]->getValue()) ?? "";
                    $data['salt_unit_price']   = ($cells[25]->getValue()) ?? "";
                    $data['sugar_unit_price']   = ($cells[26]->getValue()) ?? "";
                    $data['total']   = ($cells[27]->getValue()) ?? "";
                    $this->orderRepository->save($data);
                    $data = [];
                }
            }
            $reader->close();
            return response()->json([
                'success' => 'SUCCESS'
            ], 200);
        } catch (Exception $e) {
            Log::error('Excel Entry Error: ' . $e->getMessage());
            return response()->json([
                'success' => 'Failed'
            ], 500);
        }
    }

    public function findOne($id)
    {
        return $this->orderRepository->findOne($id);
    }

    public function update($data, $id)
    {
        $order = $this->orderRepository->findOne($id);
        return $order->update($data);
    }
}
