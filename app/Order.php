<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['id', 'ordered_time', 'factory_name', 'worker_name', 'worker_id', 'worker_phone', 'supervisor_name',
        'supervisor_id', '25_kg_rice_bag_28', '25_kg_rice_bag_28_unit_price', '25_kg_rice_bag_29', '25_kg_rice_bag_29_unit_price',
        'oil_2_liter', 'oil_2_liter_unit_price', 'oil_5_liter', 'oil_5_liter_unit_price', 'Lentils', 'Lentils_unit_price',
        'flour', 'flour_unit_price', 'potatoes', 'potatoes_unit_price', 'onion', 'onion_unit_price', 'salt', 'salt_unit_price',
        'sugar', 'sugar_unit_price', 'total', 'status'
    ];
}
